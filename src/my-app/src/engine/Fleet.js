// Fleet object
// This object is used to keep track of a player's portfolio of ships
// Constructor
import {CONST} from "./engine";
import {Ship} from "./Ship";

export function Fleet(playerGrid, fleetRoster) {
    this.playerGrid = playerGrid;
    this.fleetRoster = [];
    this.populate(fleetRoster);
}
// Populates a fleet
Fleet.prototype.populate = function(fleetRoster) {
    fleetRoster.forEach((ship) => {
        this.fleetRoster.push(new Ship(ship.type, this.playerGrid));
    });
};
// // Places the ship and returns whether or not the placement was successful
// // Returns boolean
// Fleet.prototype.placeShip = function(x, y, direction, shipType) {
//     let shipCoords;
//     for (let i = 0; i < this.fleetRoster.length; i++) {
//         let shipTypes = this.fleetRoster[i].type;
//
//         if (shipType === shipTypes &&
//             this.fleetRoster[i].isLegal(x, y, direction)) {
//             this.fleetRoster[i].create(x, y, direction, false);
//             shipCoords = this.fleetRoster[i].getAllShipCells();
//
//             for (let j = 0; j < shipCoords.length; j++) {
//                 this.playerGrid.updateCell(shipCoords[j].x, shipCoords[j].y, 'ship', this.player);
//             }
//             return true;
//         }
//     }
//     return false;
// };
// Places ships randomly on the board
// TODO: Avoid placing ships too close to each other
// Fleet.prototype.placeShipsRandomly = function() {
//     let shipCoords;
//     for (let i = 0; i < this.fleetRoster.length; i++) {
//         let illegalPlacement = true;
//
//         // Prevents the random placement of already placed ships
//         if(this.player === CONST.HUMAN_PLAYER && Game.usedShips[i] === CONST.USED) {
//             continue;
//         }
//         while (illegalPlacement) {
//             let randomX = Math.floor(Game.size * Math.random());
//             let randomY = Math.floor(Game.size * Math.random());
//             let randomDirection = Math.floor(2*Math.random());
//
//             if (this.fleetRoster[i].isLegal(randomX, randomY, randomDirection)) {
//                 this.fleetRoster[i].create(randomX, randomY, randomDirection, false);
//                 shipCoords = this.fleetRoster[i].getAllShipCells();
//                 illegalPlacement = false;
//             } else {
//                 continue;
//             }
//         }
//         if (this.player === CONST.HUMAN_PLAYER && Game.usedShips[i] !== CONST.USED) {
//             for (let j = 0; j < shipCoords.length; j++) {
//                 this.playerGrid.updateCell(shipCoords[j].x, shipCoords[j].y, 'ship', this.player);
//                 Game.usedShips[i] = CONST.USED;
//             }
//         }
//     }
// };
// Finds a ship by location
// Returns the ship object located at (x, y)
// If no ship exists at (x, y), this returns null instead
Fleet.prototype.findShipByCoords = function(x, y) {
    for (let i = 0; i < this.fleetRoster.length; i++) {
        let currentShip = this.fleetRoster[i];
        if (currentShip.direction === Ship.DIRECTION_VERTICAL) {
            if (y === currentShip.yPosition &&
                x >= currentShip.xPosition &&
                x < currentShip.xPosition + currentShip.shipLength) {
                return currentShip;
            }
        } else {
            if (x === currentShip.xPosition &&
                y >= currentShip.yPosition &&
                y < currentShip.yPosition + currentShip.shipLength) {
                return currentShip;
            }
        }
    }

    return null;
};
// Finds a ship by its type
// Param shipType is a string
// Returns the ship object that is of type shipType
// If no ship exists, this returns null.
// Fleet.prototype.findShipByType = function(shipType) {
//     for (let i = 0; i < this.fleetRoster.length; i++) {
//         if (this.fleetRoster[i].type === shipType) {
//             return this.fleetRoster[i];
//         }
//     }
//     return null;
// };
// Checks to see if all ships have been sunk
// Returns boolean
Fleet.prototype.allShipsSunk = function() {
    for (let i = 0; i < this.fleetRoster.length; i++) {
        // If one or more ships are not sunk, then the sentence "all ships are sunk" is false.
        if (this.fleetRoster[i].sunk === false) {
            return false;
        }
    }
    return true;
};