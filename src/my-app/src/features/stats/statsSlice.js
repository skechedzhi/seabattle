import {createSlice} from '@reduxjs/toolkit';

// import { current } from '@reduxjs/toolkit'

const initialState = {
    stats: [],
    show: false,
    useOpenings: false,
};

export const statsSlice = createSlice({
    name: 'stats',
    initialState,
    reducers: {
        showStats: (state, action) => {
            if (action.payload.stats) {
                state.stats = action.payload.stats
            }
            state.show = true;
        },
        hideStats: (state, action) => {
            state.show = false;
        },
        toggleUseOpenings: (state, action) => {
            state.useOpenings = !state.useOpenings
        }
    },
});

export const {showStats, hideStats, toggleUseOpenings} = statsSlice.actions;
export const selectStats = (state) => state.stats.stats
export const selectStatsVisibility = (state) => state.stats.show
export const selectUseOpenings = (state) => state.stats.useOpenings

export default statsSlice.reducer;
