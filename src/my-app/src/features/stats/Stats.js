import styles from './Stats.module.scss'
import {useDispatch, useSelector} from "react-redux";
import {hideStats, selectStatsVisibility, selectUseOpenings, showStats, toggleUseOpenings} from "./statsSlice";
import {getStatsProbabilities} from "../../engine/brain/stats";

export function Stats() {
    const dispatch = useDispatch()
    const statsVisible = useSelector(selectStatsVisibility)
    const statsUseOpenings = useSelector(selectUseOpenings)

    function toggle() {
        if (statsVisible) {
            dispatch(hideStats())
        } else {
            const stats = getStatsProbabilities(statsUseOpenings)
            dispatch(showStats({stats}))
        }
    }

    function toggleOpenings() {
        if (statsVisible) {
            const openings = getStatsProbabilities(!statsUseOpenings)
            dispatch(showStats({stats: openings}))
        }
        dispatch(toggleUseOpenings())
    }

    return <div className={styles.stats}>
        <a href="#" onClick={toggle}>{statsVisible ? 'Hide' : 'Show'}</a>
        <span> Ship statistics on the Field. </span>
        <span className={statsVisible ? '' : styles.hidden}>
            <input type="checkbox"
                   checked={!!statsUseOpenings}
                   onChange={toggleOpenings}
            />
            <span> Add Opening Stats</span>
        </span>
    </div>
}