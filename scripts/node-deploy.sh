#!/bin/bash

PROJECT_NAME="seabattle"

cd /volume1/node/${PROJECT_NAME}/release
release_dir="release_$(date +'%m_%d_%Y')"
mkdir ${release_dir}
tar -xzvf release_artifact.tar.gz --directory ${release_dir}

cd ${release_dir}
pwd
#rm .env.local
#ln -s ../shared/.env.local .env.local

/usr/local/bin/npm run build || exit 1
~/tools/pm2/node_modules/pm2/bin/pm2 delete ${PROJECT_NAME}
~/tools/pm2/node_modules/pm2/bin/pm2 start npm --name "${PROJECT_NAME}" -- start

cd ..
[ -d current ] && rm -r current
ln -s ${release_dir} current
