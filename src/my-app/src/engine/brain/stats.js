
// AI's opening book.
// This is the pattern of the first cells for the AI to target
import {loadStats, updateStatsFor} from "./storage";

const AI_OPENINGS = [
    {'x': 7, 'y': 3, 'stat': '3'},
    {'x': 6, 'y': 2, 'stat': '3'},
    {'x': 3, 'y': 7, 'stat': '3'},
    {'x': 2, 'y': 6, 'stat': '3'},
    {'x': 6, 'y': 6, 'stat': '3'},
    {'x': 3, 'y': 3, 'stat': '3'},
    {'x': 5, 'y': 5, 'stat': '3'},
    {'x': 4, 'y': 4, 'stat': '3'},
    {'x': 0, 'y': 8, 'stat': '6'},
    {'x': 1, 'y': 9, 'stat': '9'},
    {'x': 8, 'y': 0, 'stat': '6'},
    {'x': 9, 'y': 1, 'stat': '9'},
    {'x': 9, 'y': 9, 'stat': '9'},
    {'x': 0, 'y': 0, 'stat': '9'}
];

function normalize(dot) {
    dot.stat = parseInt(dot.stat || 0);
    return dot;
}

function addOpeningsTo(dots) {
    AI_OPENINGS.map(openingProb => {
        openingProb = normalize(openingProb);
        const existingDot = dots.find(dot => dot.x == openingProb.x && dot.y == openingProb.y)
        if (existingDot) {
            existingDot.stat += parseInt(openingProb.stat)
        } else {
            dots.push(openingProb)
        }
    })
}

export function fetchStartingProbs() {
    let dots = loadStats().filter(dot => !!dot.stat ).map(normalize);
    addOpeningsTo(dots)

    let stats = [];
    dots.map(dot => {
        stats.push(dot.stat);
    });

    const min = Math.min(...stats);
    const max = Math.max(...stats);
    const delta = Math.round((max-min) / 3);

    const probsMap = [
        { from: min-1, to: min+delta, weight: 'low' },
        { from: min+delta, to: min+(2*delta), weight: 'med' },
        { from: min+(2*delta), to: max+1, weight: 'high' }
    ]

    const getWeight = function (value) {
        const dot = probsMap.find(w => w.from < value && value <= w.to);

        return dot.weight;
    }

    let openings = dots.map(dot => {
        return {
            x: dot.x,
            y: dot.y,
            weight: getWeight(dot.stat)
        }
    })

    return openings;
}

export function storeShip(ship) {
    updateStatsFor(ship);
}

export function modifyProbability(dot, delta) {
    updateStatsFor([dot], delta);
}

export function getStatsProbabilities(withOpenings) {
    let max = 0;
    let dots = loadStats().map(normalize);
    if (withOpenings) {
        addOpeningsTo(dots)
    }
    dots.map(dot => {
        if (dot.stat > max) {
            max = dot.stat;
        }
        return dot;
    });

    const stats = {};
    dots.map(dot => {
        if (!stats[dot.x]) {
            stats[dot.x] = {}
        }
        stats[dot.x][dot.y] = dot.stat / max
    })

    return stats
}