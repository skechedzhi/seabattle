import styles from './Fleet.module.scss';
import {useDispatch, useSelector} from "react-redux";
import {addShip, selectFleet} from "./fleetSlice";
import {useState} from "react";
import {CONST} from "../../engine/engine";
import {ucfirst} from "../../engine/utils";
import {GameStatus, selectGameStatus, updateGameStatus} from "../field/fieldSlice";

export function Fleet() {
    const fleet = useSelector(selectFleet);
    const gameStatus = useSelector(selectGameStatus);
    const dispatch = useDispatch();
    const [state, setState] = useState({shipType: getShipTypes()[0]?.type});

    function shipCapacity(ship) {
        let output = [];
        for (let i=0; i<ship.capacity; i++) {
            let hit = i >
            output.push(<span className={`${styles.hitstart} ' ' ${i < ship.damage ? styles.damaged : ''}`} key={i}>*</span>)
        }

        return output;
    }

    function getShipTypes () {
        return CONST.SHIP_TYPES;
    }

    function renderForm () {
        if (gameStatus === GameStatus.PREPARE || gameStatus === GameStatus.STARTING) {
            return <div className={styles.editForm}>
                <div>
                    <select name="type" value={state.type} onChange={(e) => { setState({ shipType: e.target.value}); }}>
                        {getShipTypes().map((shipType, id) =>
                            <option key={id} value={shipType.type}>{ucfirst(shipType.type)} {`(${shipType.length})`}</option>
                        )}
                    </select>
                </div>
                <div>
                    <button
                        className={styles.button}
                        onClick={() => onAddFleet()}
                    >
                        Add Ship
                    </button>
                </div>
            </div>
        }

        return ''
    }

    function onAddFleet() {
        dispatch(addShip({ type: state.shipType }))
        dispatch(updateGameStatus(GameStatus.PREPARE))
    }

    return (
        <div className="fleet">
            <h2>Fleet</h2>
            <div className={styles.fleet}>
                {fleet.map((ship) =>
                    <div className={ship.status === 'sunk' ? styles.sunk : ''} key={ship.id}>
                        <span className={styles.name}>{ucfirst(ship.type)}</span>
                        <div>{shipCapacity(ship)}</div>
                    </div>
                )}
            </div>
            {renderForm()}
        </div>
    );
}
