import {useSelector, useDispatch} from 'react-redux';
import styles from './Field.module.scss';
import {
    fire,
    hit,
    updateGameStatus,
    selectCells,
    selectGameStatus
} from './fieldSlice';
import { confirmAlert } from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css';
import {createAI, getNextMove, shotResult} from "../../engine/engine";
import {hitShip, killShip, selectFleet} from "../fleet/fleetSlice";
import {Victory} from "../victory/Victory";
import {Marks} from "../marks/Marks";
import {storeShip} from "../../engine/brain/stats";
import {selectStats, selectStatsVisibility} from "../stats/statsSlice";
import {Cell} from "../cell/Cell";

export function Field() {
    const dispatch = useDispatch();
    const cells = useSelector(selectCells);
    const gameStatus = useSelector(selectGameStatus);
    const fleet = useSelector(selectFleet);
    const stats = useSelector(selectStats)
    const statsVisible = useSelector(selectStatsVisibility)

    const onCellClick = (cell, x, y) => {

    }

    const onConfirmClick = (coordinates, result) => {
        dispatch(hit({...coordinates, state: result}))
        const res = shotResult(coordinates, result);

        if (result === 'hit') {
            dispatch(hitShip(res.shipCoordinates.length));
        }
        if (result === 'sunk') {
            let toUpdate = [];
            if (res.shipCoordinates.length) {
                toUpdate.push({ state: 'sunk', cells: res.shipCoordinates });
                storeShip(res.shipCoordinates);
            }
            if (res.surroundings.length) {
                toUpdate.push({ state: 'miss', cells: res.surroundings });
            }
            toUpdate.forEach(action => {
                action.cells.forEach(cell => {
                    dispatch(hit({...cell, state: action.state}))
                })
            });

            dispatch(killShip(res.shipCoordinates.length));
            if (fleet.filter(ship => ship.status !== 'sunk').length === 1) {
                dispatch(updateGameStatus('over'))
            }
        }
    }
    const checkIsHit = (coordinates) => {
        let result = 0;

        confirmAlert({
            title: `Firing to ${marks['horizontal'][coordinates.x]}:${marks['vertical'][coordinates.y].toUpperCase()}`,
            message: 'Was it a hit?',
            buttons: [
                {
                    label: 'Hit',
                    onClick: () => onConfirmClick(coordinates, 'hit')
                },
                {
                    label: 'Miss',
                    onClick: () => onConfirmClick(coordinates, 'miss')
                },
                {
                    label: 'Sunk',
                    onClick: () => onConfirmClick(coordinates, 'sunk')
                }
            ]
        })

        return result;
    }

    const onFire = () => {
        const coordinates = getNextMove();
        dispatch(fire(coordinates))

        checkIsHit(coordinates);
    }

    const onStart = () => {
        dispatch(updateGameStatus('ongoing'))
        createAI(fleet)
    }

    const renderButton = () => {
        if (gameStatus === 'prepare') {
            return <button
                    className={styles.button}
                    onClick={() => onStart()}
                >
                    Start Game
                </button>
        } else if (gameStatus === 'ongoing') {
            return <button
                className={styles.button}
                onClick={() => onFire()}
            >
                Fire!
            </button>
        }
    }

    const marks = {
        horizontal: [1,2,3,4,5,6,7,8,9,10],
        vertical: ['а', 'б', 'в', 'г', 'д', 'е', 'ж', 'з', 'и', 'к'],
    }

    return (
        <div>
            <Victory />
            <Marks marks={marks.horizontal} dir='horizontal' />
            <div className={styles.boardWrapper}>
                <Marks marks={marks.vertical} dir='vertical' />
                <div className={styles.field}>
                    {cells.map((row, y) =>
                        <div className={styles.row} key={y}>
                            {row.map((cell, x) => <Cell
                                    onCellClick={onCellClick}
                                    status={cell}
                                    x={x}
                                    y={y}
                                    stat={stats[x] ? stats[x][y] : undefined}
                                    statsVisible={statsVisible}
                                    key={x}
                                />
                            )}
                        </div>
                    )}
                </div>
            </div>
            <div className={styles.buttonSet}>
                {renderButton()}
            </div>
        </div>
    );
}
