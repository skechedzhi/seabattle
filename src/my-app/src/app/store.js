import { configureStore } from '@reduxjs/toolkit';
import fieldReducer from '../features/field/fieldSlice';
import fleetReducer from '../features/fleet/fleetSlice';
import statsReducer from '../features/stats/statsSlice';

export const store = configureStore({
  reducer: {
    field: fieldReducer,
    fleet: fleetReducer,
    stats: statsReducer,
  },
});
