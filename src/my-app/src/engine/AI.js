import {Fleet} from "./Fleet";
import {getRandom, getSurroundings} from "./utils";
import {Ship} from "./Ship";
import {CONST, detectShipType, Game} from "./engine";
import {fetchStartingProbs} from "./brain/stats";

function Grid (size) {
    return {
        cells: Array(size).fill(0).map(() => Array(size).fill(0))
    }
}

// AI Object
// Optimal battleship-playing AI
// Constructor
export function AI(ships) {
    this.virtualGrid = new Grid(Game.size);
    this.virtualFleet = new Fleet(this.virtualGrid, ships);

    this.probGrid = []; // Probability Grid
    this.initProbs();
    this.updateProbs();
}

AI.PROB_WEIGHT = 5000; // arbitrarily big number
// how much weight to give to the opening book's high probability cells
AI.OPEN_HIGH_MIN = 20;
AI.OPEN_HIGH_MAX = 30;
// how much weight to give to the opening book's medium probability cells
AI.OPEN_MED_MIN = 15;
AI.OPEN_MED_MAX = 25;
// how much weight to give to the opening book's low probability cells
AI.OPEN_LOW_MIN = 10;
AI.OPEN_LOW_MAX = 20;
// Amount of randomness when selecting between cells of equal probability
AI.RANDOMNESS = 0.1;
// Scouts the grid based on max probability, and shoots at the cell
// that has the highest probability of containing a ship
AI.prototype.shoot = function () {
    let maxProbability = 0;
    let maxProbCoords;
    let maxProbs = [];

    // Add the AI's opening book to the probability grid
    for (let i = 0; i < this.openings.length; i++) {
        let cell = this.openings[i];
        if (this.probGrid[cell.x][cell.y] !== 0) {
            this.probGrid[cell.x][cell.y] += cell.weight;
        }
    }

    for (let x = 0; x < Game.size; x++) {
        for (let y = 0; y < Game.size; y++) {
            if (this.probGrid[x][y] > maxProbability) {
                maxProbability = this.probGrid[x][y];
                maxProbs = [{'x': x, 'y': y}]; // Replace the array
            } else if (this.probGrid[x][y] === maxProbability) {
                maxProbs.push({'x': x, 'y': y});
            }
        }
    }

    maxProbCoords = Math.random() < AI.RANDOMNESS ?
        maxProbs[Math.floor(Math.random() * maxProbs.length)] :
        maxProbs[0];

    return maxProbCoords;
}

AI.prototype.shotResult = function (maxProbCoords, result) {
    this.virtualGrid.cells[maxProbCoords.x][maxProbCoords.y] = result;

    let shipCoordinates = [], surroundings = [];
    shipCoordinates = this.findShipByPoint(maxProbCoords.x, maxProbCoords.y);
    if (result === CONST.TYPE_SUNK) {
        let cells = this.virtualGrid.cells;
        shipCoordinates.forEach(coords => {
            cells[coords.x][coords.y] = CONST.TYPE_SUNK;
        });

        surroundings = getSurroundings(shipCoordinates);
        surroundings.forEach(coords => { cells[coords.x][coords.y] = CONST.TYPE_MISS; });


        // Remove any ships from the roster that have been sunk
        let shipTypes = [];
        for (let k = 0; k < this.virtualFleet.fleetRoster.length; k++) {
            shipTypes.push(this.virtualFleet.fleetRoster[k].type);
        }
        let index = shipTypes.indexOf(detectShipType(shipCoordinates));
        this.virtualFleet.fleetRoster.splice(index, 1);
    }

    // Update probability grid after each shot
    this.updateProbs();

    return { shipCoordinates, surroundings }
};
// Update the probability grid
AI.prototype.updateProbs = function () {
    let roster = this.virtualFleet.fleetRoster;
    let coords;
    this.resetProbs();

    // Probabilities are not normalized to fit in the interval [0, 1]
    // because we're only interested in the maximum value.

    // This works by trying to fit each ship in each cell in every orientation
    // For every cell, the more legal ways a ship can pass through it, the more
    // likely the cell is to contain a ship.
    // Cells that surround known 'hits' are given an arbitrarily large probability
    // so that the AI tries to completely sink the ship before moving on.

    // TODO: Think about a more efficient implementation
    for (let k = 0; k < roster.length; k++) {
        // Skip small ships
        // if (roster[k].maxDamage < 2) {
        //     continue;
        // }
        for (let x = 0; x < Game.size; x++) {
            for (let y = 0; y < Game.size; y++) {
                if (roster[k].isLegal(x, y, Ship.DIRECTION_VERTICAL)) {
                    roster[k].create(x, y, Ship.DIRECTION_VERTICAL, true);
                    coords = roster[k].getAllShipCells();
                    let hitCellsCovered = this.numHitCellsCovered(coords);
                    if (hitCellsCovered) {
                        for (let i = 0; i < coords.length; i++) {
                            this.probGrid[coords[i].x][coords[i].y] += Math.pow(AI.PROB_WEIGHT,  hitCellsCovered);
                        }
                    } else {
                        for (let _i = 0; _i < coords.length; _i++) {
                            this.probGrid[coords[_i].x][coords[_i].y]++;
                        }
                    }
                }
                if (roster[k].isLegal(x, y, Ship.DIRECTION_HORIZONTAL)) {
                    roster[k].create(x, y, Ship.DIRECTION_HORIZONTAL, true);
                    coords = roster[k].getAllShipCells();
                    let hitCellsCovered = this.numHitCellsCovered(coords);
                    if (hitCellsCovered) {
                        for (let j = 0; j < coords.length; j++) {
                            this.probGrid[coords[j].x][coords[j].y] += Math.pow(AI.PROB_WEIGHT,  hitCellsCovered);
                        }
                    } else {
                        for (let _j = 0; _j < coords.length; _j++) {
                            this.probGrid[coords[_j].x][coords[_j].y]++;
                        }
                    }
                }

                // Set hit cells to probability zero so the AI doesn't
                // target cells that are already hit
                if (this.virtualGrid.cells[x][y] === CONST.TYPE_HIT) {
                    this.probGrid[x][y] = 0;
                }
            }
        }
    }
};
// Initializes the probability grid for targeting
AI.prototype.initProbs = function () {
    for (let x = 0; x < Game.size; x++) {
        let row = [];
        this.probGrid[x] = row;
        for (let y = 0; y < Game.size; y++) {
            row.push(0);
        }
    }

    function clculateWeight (weight) {
        let weightNumber;

        switch (weight) {
            case 'high': weightNumber = getRandom(AI.OPEN_HIGH_MIN, AI.OPEN_HIGH_MAX); break;
            case 'med': weightNumber = getRandom(AI.OPEN_MED_MIN, AI.OPEN_MED_MAX); break;
            case 'low': weightNumber = getRandom(AI.OPEN_LOW_MIN, AI.OPEN_LOW_MAX); break;
        }

        return weightNumber;
    }
    let openings = [];
    fetchStartingProbs().forEach(prob => {
        openings.push({
            x: prob.x,
            y: prob.y,
            weight: clculateWeight(prob.weight)
        });

    })
    this.openings = openings;
};
// Resets the probability grid to all 0.
AI.prototype.resetProbs = function () {
    for (let x = 0; x < Game.size; x++) {
        for (let y = 0; y < Game.size; y++) {
            this.probGrid[x][y] = 0;
        }
    }
};
// AI.prototype.metagame = function () {
//     // Inputs:
//     // Proximity of hit cells to edge
//     // Proximity of hit cells to each other
//     // Edit the probability grid by multiplying each cell with a new probability weight (e.g. 0.4, or 3). Set this as a CONST and make 1-CONST the inverse for decreasing, or 2*CONST for increasing
// };
// Finds a human ship by coordinates
// Returns Ship
AI.prototype.findShipByPoint = function (x, y) {
    if (this.virtualGrid.cells[x][y] !== CONST.TYPE_HIT && this.virtualGrid.cells[x][y] !== CONST.TYPE_SUNK) {
        return [];
    }

    let coords = [{x,y}];
    coords = coords.concat(this.lookupCoordinates(x, y, Ship.DIRECTION_HORIZONTAL, 1))
    coords = coords.concat(this.lookupCoordinates(x, y, Ship.DIRECTION_HORIZONTAL, -1))
    coords = coords.concat(this.lookupCoordinates(x, y, Ship.DIRECTION_VERTICAL, 1))
    coords = coords.concat(this.lookupCoordinates(x, y, Ship.DIRECTION_VERTICAL, -1))

    return coords;
};
AI.prototype.lookupCoordinates = function (x, y, orientation, direction) {
    let coords = [];
    let limit = 10000;
    let _x = x, _y = y;
    while (limit--) {
        if (orientation === Ship.DIRECTION_HORIZONTAL) {
            _x += direction;
        } else {
            _y += direction;
        }

        if (typeof this.virtualGrid.cells[_x] === 'undefined'
            || typeof this.virtualGrid.cells[_x][_y] === 'undefined'
            || (this.virtualGrid.cells[_x][_y] !== CONST.TYPE_HIT && this.virtualGrid.cells[_x][_y] !== CONST.TYPE_SUNK)
        ) {
            break;
        }

        coords.push({x:_x, y:_y});
    }

    return coords;
}
// Gives the number of hit cells the ships passes through. The more
// cells this is, the more probable the ship exists in those coordinates
// Returns int
AI.prototype.numHitCellsCovered = function (shipCells) {
    let cells = 0;
    for (let i = 0; i < shipCells.length; i++) {
        if (this.virtualGrid.cells[shipCells[i].x][shipCells[i].y] === CONST.TYPE_HIT) {
            cells++;
        }
    }
    return cells;
};