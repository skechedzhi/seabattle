import styles from './Cell.module.scss'
import {useEffect, useRef, useState} from "react";
import {Hover} from "./hover/Hover";

export function Cell(props) {

    const [state, setState] = useState({hover: false});

    function cellStatusClass() {
        let stateClass = ''
        switch (props.status) {
            case 'hit': stateClass = styles.hit; break;
            case 'miss': stateClass = styles.miss; break;
            case 'fire': stateClass = styles.fire; break;
            case 'sunk': stateClass = styles.sunk; break;
        }

        return stateClass;
    }

    function cellClass () {
        return [
            styles.cell,
            (state.hover ? styles.hovered : ''),
            cellStatusClass(),
        ]
    }

    function statsStyle() {
        if (!props.statsVisible) {
            return {}
        }

        return { 'backgroundColor': `RGBA(100,100,100,${props.stat || 0})` }
    }

    function onCellClick() {
        if (props.statsVisible) {
            setState({hover: true})
        }
    }

    return <div className={styles.cellContainer}>
        <div className={cellClass().join(' ')}
                    style={statsStyle()}
                    onClick={(event) => onCellClick()}
                    title={`X:${props.x}, Y:${props.y}${props.statsVisible ? ', ' + Math.round(props.stat*100) + '%' : ''}`}
        >
            {' '}
        </div>
        {!state.hover ? null : <Hover hide={() => setState({hover:false})} x={props.x} y={props.y} />}
    </div>
}