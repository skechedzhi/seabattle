import styles from "./Victory.module.scss";
import {useSelector} from "react-redux";
import {selectGameStatus} from "../field/fieldSlice";

export function Victory () {

    const gameStatus = useSelector(selectGameStatus)

    if (gameStatus === 'over') {
        return <div className={styles.victory}>
            <span>Victory!</span>
        </div>
    }

    return ''
}