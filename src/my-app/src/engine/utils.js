// Returns a random number between min (inclusive) and max (exclusive)
import {Game} from "./engine";

export function getRandom(min, max) {
    return Math.random() * (max - min) + min;
}

/**
 * @param coordinates Array
 */
export function getSurroundings(coordinates) {
    let surroundings = []
    coordinates.forEach(coord => {
        surroundings = surroundings.concat(getAround(coord));
    });

    let filteredSurroundings = [];
    surroundings.forEach((value) => {
        if (!in_array(value, coordinates) && !in_array(value, filteredSurroundings)) {
            filteredSurroundings.push(value);
        }
    });

    return filteredSurroundings;
}

function getAround(coords) {
    const aroundCB = [
        (coord) => { return {x:coord.x-1, y:coord.y}}, // left
        (coord) => { return {x:coord.x-1, y:coord.y-1}}, // left-top
        (coord) => { return {x:coord.x, y:coord.y-1}}, // top
        (coord) => { return {x:coord.x+1, y:coord.y-1}}, // top-right
        (coord) => { return {x:coord.x+1, y:coord.y}}, // right
        (coord) => { return {x:coord.x+1, y:coord.y+1}}, // bottom-right
        (coord) => { return {x:coord.x, y:coord.y+1}}, // bottom
        (coord) => { return {x:coord.x-1, y:coord.y+1}}, // bottom-left

    ];
    const around = aroundCB.map(cb => {
        return cb(coords);
    });

    return around.filter(value => { return value.x >= 0 && value.y >= 0 && value.x < Game.size && value.y < Game.size; });
}

/**
 * @param needle Coordinates
 * @param hyStack Array
 */
export function in_array(needle, hyStack) {
    return !!hyStack.find(value => { return needle.x == value.x && needle.y == value.y; });
}

export function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}