import React from 'react';
import './App.css';
import {Field} from "./features/field/Field";
import {Fleet} from "./features/fleet/Fleet";
import {Stats} from "./features/stats/Stats";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <div className="app-horizontal">
          <div>
            <Field />
          </div>
          <div>
            <Fleet />
          </div>
        </div>
        <Stats />
        <span>
          <span>Learn </span>
          <a
            className="App-link"
            href="https://reactjs.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux
          </a>
          <span>, </span>
          <a
            className="App-link"
            href="https://redux-toolkit.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            Redux Toolkit
          </a>
          ,<span> and </span>
          <a
            className="App-link"
            href="https://react-redux.js.org/"
            target="_blank"
            rel="noopener noreferrer"
          >
            React Redux
          </a>
        </span>
      </header>
    </div>
  );
}

export default App;
