import styles from './Hover.module.scss'
import {useEffect, useRef} from "react";
import {getStatsProbabilities, modifyProbability} from "../../../engine/brain/stats";
import {selectUseOpenings, showStats} from "../../stats/statsSlice";
import {useDispatch, useSelector} from "react-redux";

export function Hover(props) {
    const dispatch = useDispatch()
    const statsUseOpenings = useSelector(selectUseOpenings)
    const wrapperRef = useRef(null);

    useEffect(() => {
        function handleClickOutside(event) {
            if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
                props.hide()
            }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
            // Unbind the event listener on clean up
            document.removeEventListener("mousedown", handleClickOutside);
        };
    }, [wrapperRef]);

    function decreaseProbability() {
        modifyProbability({x:props.x, y:props.y}, -1);
        refreshStats();
    }
    function increaseProbability() {
        modifyProbability({x:props.x, y:props.y}, +1);
        refreshStats();
    }

    function refreshStats() {
        const openings = getStatsProbabilities(statsUseOpenings)
        dispatch(showStats({stats: openings}))
    }

    return <div className={styles.hover} ref={wrapperRef}>
        <button onClick={decreaseProbability}> - </button>
        <span> Probability </span>
        <button onClick={increaseProbability}> + </button>
    </div>
}