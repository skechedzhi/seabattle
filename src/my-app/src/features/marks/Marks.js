import styles from './Marks.module.scss'

export function Marks (props) {

    function getClassName() {
        return [
            styles.marks,
            props.dir === 'horizontal' ? styles.horizontal : '',
            props.dir === 'vertical' ? styles.vertical : '',
        ].join(' ');
    }

    return <div className={getClassName()}>
        {(props.marks || []).map((letter, i) => <span className={styles.letter} key={i}>{letter}</span> )}
    </div>
}