PROJECT_NAME=seabattle

start:
	docker-compose up --remove-orphans

reload: stop start

stop:
	docker-compose down

shell:
	docker-compose run --rm web /bin/bash

### Server part
connect:
	ssh stkecnas@79.171.125.84 -p24