/* eslint-disable no-restricted-globals */

let CACHE_NAME = 'seabattle-pwa';
let urlsToCache = [
    '/',
    '/manifest.json',
    '/favicon.ico',
    '/logo192.png',
    '/logo512.png'
];
self.__WB_MANIFEST.map(entry => {
    urlsToCache.push(entry.url)
})

// Install a service worker
self.addEventListener('install', event => {
    // Perform install steps
    event.waitUntil(
        caches.open(CACHE_NAME)
            .then(function(cache) {
                console.log('Opened cache');
                return cache.addAll(urlsToCache);
            })
    );
});

// Cache and return requests
self.addEventListener('fetch', event => {
    event.respondWith(
        caches.match(event.request)
            .then(function(response) {
                    // Cache hit - return response
                    if (response) {
                        return response;
                    }
                    return fetch(event.request);
                }
            )
    );
});

// Update a service worker
self.addEventListener('activate', event => {
    let cacheWhitelist = [CACHE_NAME];
    event.waitUntil(
        caches.keys().then(cacheNames => {
            return Promise.all(
                cacheNames.map(cacheName => {
                    if (cacheWhitelist.indexOf(cacheName) === -1) {
                        return caches.delete(cacheName);
                    }
                })
            );
        })
    );
});