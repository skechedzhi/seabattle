import {CONST, Game} from "./engine";

export function Ship(type, playerGrid) {
    const shipType = CONST.SHIP_TYPES.find(shipType => {
        return shipType.type == type;
    });
    this.damage = 0;
    this.type = type;
    this.playerGrid = playerGrid;
    this.maxDamage = this.shipLength = shipType.length;
    this.sunk = false;
}
// Checks to see if the placement of a ship is legal
// Returns boolean
Ship.prototype.isLegal = function(x, y, direction) {
    // first, check if the ship is within the grid...
    if (this.withinBounds(x, y, direction)) {
        // ...then check to make sure it doesn't collide with another ship
        for (let i = 0; i < this.shipLength; i++) {
            if (direction === Ship.DIRECTION_VERTICAL) {
                if (this.playerGrid.cells[x + i][y] === CONST.TYPE_SHIP ||
                    this.playerGrid.cells[x + i][y] === CONST.TYPE_MISS ||
                    this.playerGrid.cells[x + i][y] === CONST.TYPE_SUNK) {
                    return false;
                }
            } else {
                if (this.playerGrid.cells[x][y + i] === CONST.TYPE_SHIP ||
                    this.playerGrid.cells[x][y + i] === CONST.TYPE_MISS ||
                    this.playerGrid.cells[x][y + i] === CONST.TYPE_SUNK) {
                    return false;
                }
            }
        }
        return true;
    } else {
        return false;
    }
};
// Checks to see if the ship is within bounds of the grid
// Returns boolean
Ship.prototype.withinBounds = function(x, y, direction) {
    if (direction === Ship.DIRECTION_VERTICAL) {
        return x + this.shipLength <= Game.size;
    } else {
        return y + this.shipLength <= Game.size;
    }
};
// // Increments the damage counter of a ship
// // Returns Ship
// Ship.prototype.incrementDamage = function() {
//     this.damage++;
//     if (this.isSunk()) {
//         this.sinkShip(false); // Sinks the ship
//     }
// };
// Checks to see if the ship is sunk
// Returns boolean
Ship.prototype.isSunk = function() {
    return this.damage >= this.maxDamage;
};
// Sinks the ship
// Ship.prototype.sinkShip = function(virtual) {
//     this.damage = this.maxDamage; // Force the damage to exceed max damage
//     this.sunk = true;
//
//     // Make the CSS class sunk, but only if the ship is not virtual
//     if (!virtual) {
//         let allCells = this.getAllShipCells();
//         for (let i = 0; i < this.shipLength; i++) {
//             this.playerGrid.cells[allCells[i].x][allCells[i].y] = CONST.TYPE_SUNK;
//         }
//     }
// };
/**
 * Gets all the ship cells
 *
 * Returns an array with all (x, y) coordinates of the ship:
 * e.g.
 * [
 *	{'x':2, 'y':2},
 *	{'x':3, 'y':2},
 *	{'x':4, 'y':2}
 * ]
 */
Ship.prototype.getAllShipCells = function() {
    let resultObject = [];
    for (let i = 0; i < this.shipLength; i++) {
        if (this.direction === Ship.DIRECTION_VERTICAL) {
            resultObject[i] = {'x': this.xPosition + i, 'y': this.yPosition};
        } else {
            resultObject[i] = {'x': this.xPosition, 'y': this.yPosition + i};
        }
    }
    return resultObject;
};
// Initializes a ship with the given coordinates and direction (bearing).
// If the ship is declared "virtual", then the ship gets initialized with
// its coordinates but DOESN'T get placed on the grid.
Ship.prototype.create = function(x, y, direction, virtual) {
    // This function assumes that you've already checked that the placement is legal
    this.xPosition = x;
    this.yPosition = y;
    this.direction = direction;

    // If the ship is virtual, don't add it to the grid.
    if (!virtual) {
        for (let i = 0; i < this.shipLength; i++) {
            if (this.direction === Ship.DIRECTION_VERTICAL) {
                this.playerGrid.cells[x + i][y] = CONST.TYPE_SHIP;
            } else {
                this.playerGrid.cells[x][y + i] = CONST.TYPE_SHIP;
            }
        }
    }

};
Ship.prototype.toDTO = function () {
    return {
        id: this.id,
        type: this.type,
        damage: this.damage,
        capacity: this.maxDamage,
    }
}
// direction === 0 when the ship is facing north/south
// direction === 1 when the ship is facing east/west
Ship.DIRECTION_VERTICAL = 0;
Ship.DIRECTION_HORIZONTAL = 1;