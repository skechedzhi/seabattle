import {createSlice} from '@reduxjs/toolkit';
import {Ship} from "../../engine/Ship";

import { current } from '@reduxjs/toolkit'

const initialState = {
    fleet: [
        // {
        //     id: 0,
        //     type: 'carrier',
        //     damage: 0,
        //     capacity: 5,
        //     status: null,
        // },
        // {
        //     id: 1,
        //     type: 'destroyer',
        //     damage: 3,
        //     capacity: 3,
        //     status: 'sunk',
        // },
    ],
    damage: 0
};

export const fleetSlice = createSlice({
    name: 'fleet',
    initialState,
    reducers: {
        killShip: (state, action) => {
            const ship = state.fleet.find(ship => {
                return ship.status != 'sunk'
                    && ship.capacity == action.payload
                    && ship.capacity - ship.damage === 1
            });
            if (ship) {
                ship.status = 'sunk';
                ship.damage++;
                state.damage = 0;
            }
        },
        hitShip: (state, action) => {
            state.damage++;
            const shortest = state.fleet
                .filter(ship => {
                    return ship.status != 'sunk'
                        && ship.capacity > action.payload
                })
                .sort((a, b) => { return a.capacity - b.capacity; });
            const shipLength = shortest[0].capacity;

            const ship = shortest
                .filter(ship => { return ship.capacity == shipLength; })
                .sort((a, b) => { return (a.capacity - a.damage) - (b.capacity - b.damage) })[0];
            if (ship) {
                state.fleet.map(otherShip => { if (otherShip.status != 'sunk') { otherShip.damage = 0; } });
                ship.damage = state.damage;
            }

        },
        addShip: (state, action) => {
            const id = state.fleet.length;
            let ship = new Ship(action.payload.type);
            ship.id = id;
            state.fleet.push(ship.toDTO());
        },
    },
});

export const {killShip, hitShip, addShip} = fleetSlice.actions;
export const selectFleet = (state) => state.fleet.fleet

export default fleetSlice.reducer;
