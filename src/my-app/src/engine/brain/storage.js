import {Game} from "../engine";

function createKey(x, y) {
    return `${x}_${y}`
}

export function updateStatsFor(coordinates, delta) {
    delta = delta || 1;
    coordinates.forEach(dot => {
        const key = createKey(dot.x, dot.y);

        let stat = parseInt(localStorage.getItem(key) || 0);
        stat = Math.max(stat + delta, 0);
        localStorage.setItem(key, stat);
    })
}

export function loadStats() {
    let stats = [];

    const field = Array(Game.size).fill(0).map(() => Array(Game.size).fill(0))
    field.forEach((row, x) => {
        row.forEach((v, y) => {
            let stat = localStorage.getItem(createKey(x, y));
            stats.push({x, y, stat})
        })
    })

    return stats;
}