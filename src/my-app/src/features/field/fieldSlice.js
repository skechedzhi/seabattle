import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';


export const GameStatus = {
  STARTING: 'starting',
  PREPARE:  'prepare',
  ONGOING:  'ongoing',
  OVER:     'over',
};

const initialState = {
  cells: Array(10).fill(Array(10).fill(0)),
  game: {
    status: GameStatus.STARTING,
    shipsLeft: null
  }
};

// The function below is called a thunk and allows us to perform async logic. It
// can be dispatched like a regular action: `dispatch(incrementAsync(10))`. This
// will call the thunk with the `dispatch` function as the first argument. Async
// code can then be executed and other actions can be dispatched. Thunks are
// typically used to make async requests.
// export const incrementAsync = createAsyncThunk(
//   'counter/fetchCount',
//   async (amount) => {
//     const response = await fetchCount(amount);
//     // The value we return becomes the `fulfilled` action payload
//     return response.data;
//   }
// );

export const fieldSlice = createSlice({
  name: 'field',
  initialState,
  // The `reducers` field lets us define reducers and generate associated actions
  reducers: {
    fire: (state, action) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the Immer library,
      // which detects changes to a "draft state" and produces a brand new
      // immutable state based off those changes
      state.cells[action.payload.y][action.payload.x] = 'fire';
    },
    hit: (state, action) => {
      state.cells[action.payload.y][action.payload.x] = action.payload.state;
    },
    updateGameStatus: (state, action) => {
      if (action.payload) {
        state.game.status = action.payload;
      }
    }
  },
});

export const { fire, hit, updateGameStatus } = fieldSlice.actions;

export const selectCells = (state) => state.field.cells;
export const selectGameStatus = (state) => state.field.game.status;
export const selectShipsLeft = (state) => state.field.game.shipsLeft;

export default fieldSlice.reducer;
