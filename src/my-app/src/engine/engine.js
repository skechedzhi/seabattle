import {AI} from "./AI";

export const Game = {
    size: 10,
}

export let CONST = {};
CONST.AVAILABLE_SHIPS = ['carrier', 'battleship', 'destroyer', 'rocketboat', 'scout'];
CONST.SHIP_TYPES = [
    {type: 'carrier', length: 5},
    {type: 'battleship', length: 4},
    {type: 'destroyer', length: 3},
    {type: 'rocketboat', length: 2},
    {type: 'scout', length: 1}
];
// You are player 0 and the computer is player 1
// The virtual player is used for generating temporary ships
// for calculating the probability heatmap
CONST.HUMAN_PLAYER = 0;
CONST.COMPUTER_PLAYER = 1;
CONST.VIRTUAL_PLAYER = 2;
// // Possible values for the parameter `type` (string)
// CONST.CSS_TYPE_EMPTY = 'empty';
// CONST.CSS_TYPE_SHIP = 'ship';
// CONST.CSS_TYPE_MISS = 'miss';
// CONST.CSS_TYPE_HIT = 'hit';
// CONST.CSS_TYPE_SUNK = 'sunk';
// Grid code:
CONST.TYPE_EMPTY = 0; // 0 = water (empty)
CONST.TYPE_SHIP = 1; // 1 = undamaged ship
CONST.TYPE_MISS = 2; // 2 = water with a cannonball in it (missed shot)
CONST.TYPE_HIT = 3; // 3 = damaged ship (hit shot)
CONST.TYPE_SUNK = 4; // 4 = sunk ship

export function detectShipType(coords) {
    const shipType = CONST.SHIP_TYPES.find(shipType => {
        return shipType.length == coords.length;
    });

    return shipType.type;
}

let ai;
export function createAI(fleetState) {
    ai = new AI(fleetState);
}

export function getNextMove() {
    if (!ai) {
        ai = new AI();
    }

    return ai.shoot();
}

export function shotResult(coordinates, result) {
    const resultsMap = {
        'miss': CONST.TYPE_MISS,
        'hit': CONST.TYPE_HIT,
        'sunk': CONST.TYPE_SUNK,
    }
    const shotResult = ai.shotResult(coordinates, resultsMap[result]);

    return shotResult;
}